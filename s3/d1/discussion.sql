
INSERT INTO artists (name) VALUES ("Rivermaya");


INSERT INTO artists (name) VALUES ("Psy");

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Psy 6",
	"2012-1-1",
	2
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Trip",
	"1996-1-1",
	1
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Kundiman",
	234,
	"OPM"
	2
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Kisapmata",
	259,
	"OPM"
	2
);

/*
[SELECTING records]
USE music_db
*/
-- Display all columns for all songs
SELECT * FROM songs;

-- Display only the title and genre of all songs
SELECT song_name, genre FROM songs;

--Display only the title of all OPM songs

SELECT song_name FROM songs WHERE genre = "OPM";

SELECT song_name, length FROM songs WHERE length > 240 AND genre = "OPM";

/*
[UPDATING records]
*/

UPDATE songs SET length = 220 WHERE song_name = "Kundiman";
UPDATE songs SET length = 220 WHERE id = 2;

--Omitting the WHERE clause will update All rows

DELETE FROM songs WHERE genre = "OPM" AND length > 240;