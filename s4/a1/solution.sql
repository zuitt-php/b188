-- 1.
SELECT * FROM artists WHERE name LIKE "%d%";

-- 2.
SELECT * FROM songs WHERE length < 230;

-- 3.
SELECT album_title, song_name, length FROM albums JOIN songs ON albums.id = songs.album_id;

-- 4. 
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%a%" ;

-- 5. 
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- 6. 
SELECT album_title, song_name
FROM albums
RIGHT JOIN songs
ON albums.id = songs.album_id
ORDER BY album_title DESC;

SELECT album_title, song_name
FROM albums
RIGHT JOIN songs
ON albums.id = songs.album_id
ORDER BY album_title ASC;

SELECT * FROM artists
		JOIN albums ON artists.id = albums.artist_id AND albums.album_title LIKE "%a%";