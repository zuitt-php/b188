INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add Albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Fearless",
	"2008-01-01",
	3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Red",
	"2012-01-01",
	3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"A Star is Born",
	"2018-01-01",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Born This Way",
	"2011-01-01",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Purpose",
	"2015-01-01",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Believe",
	"2012-01-01",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Dangerous Woman",
	"2016-01-01",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Fearless",
	253,
	"Pop Rock",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Love Story",
	253,
	"Country pop",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"State of Grace",
	253,
	"Rock",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Red",
	204,
	"Country",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Black Eyes",
	181,
	"Rock",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Born this Way",
	252,
	"Electropop",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Sorry",
	152,
	"Dancehall-op",
	7
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Into You",
	242,
	"EDM",
	9
);

--Exclude records

SELECT * FROM songs WHERE id != 5;

SELECT * FROM songs WHERE id >= 4;

SELECT * FROM songs WHERE id <= 7;

SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 6;

SELECT * FROM songs WHERE id = 1 OR song_name = "Red" OR length < 200;

SELECT * FROM songs WHERE id IN (1,5,6);

SELECT * FROM songs WHERE song_name LIKE "%e";

SELECT * FROM songs WHERE song_name LIKE "e%";

SELECT * FROM songs WHERE song_name LIKE "%a%";

--Using toUpper case to solve case sensitive issues

SELECT UPPER(song_name) FROM songs WHERE song_name = UPPER("red");

SELECT LOWER(song_name) FROM songs WHERE song_name = LOWER("RED");

--Alphabetical ascending
SELECT * FROM songs ORDER BY song_name ASC;
--Alphabetical descending
SELECT * FROM songs ORDER BY song_name DESC;

SELECT * FROM songs ORDER BY song_name DESC LIMIT 3;

SELECT DISTINCT genre FROM songs;

--Count
SELECT COUNT(*) FROM songs WHERE genre = "Rock";

---[Table Joins]

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artist_id;

SELECT * FROM artists
	RIGHT JOIN albums ON artists.id = albums.artist_id; 

SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;